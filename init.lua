vim.g.mapleader = " "
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup(
	{
		"L3MON4D3/LuaSnip",
		{ "catppuccin/nvim", name = "catppuccin" },
		"hrsh7th/cmp-buffer",
		"hrsh7th/cmp-cmdline",
		"hrsh7th/cmp-nvim-lsp",
		"hrsh7th/cmp-path",
		"hrsh7th/cmp-vsnip",
		"saadparwaiz1/cmp_luasnip",
		"rafamadriz/friendly-snippets",
		"junegunn/fzf",
		"junegunn/fzf.vim",
		"VonHeikemen/lsp-zero.nvim",
		"williamboman/mason-lspconfig.nvim",
		"williamboman/mason.nvim",
		"hrsh7th/nvim-cmp",
		"neovim/nvim-lspconfig",
		"kyazdani42/nvim-tree.lua",
		{
			"nvim-treesitter/nvim-treesitter",
			build = ":TSUpdate",
			cmd = { "TSUpdateSync" },
			opts = {
				ensure_installed = {
					"all",
				},
			},
		},
		"nvim-treesitter/nvim-treesitter-context",
		"nvim-lua/plenary.nvim",
		"rust-lang/rust.vim",
		"nvim-telescope/telescope-fzf-native.nvim",
		"nvim-telescope/telescope.nvim",
		"akinsho/toggleterm.nvim",
		"vim-airline/vim-airline",
		"tpope/vim-commentary",
		"tpope/vim-fugitive",
		"airblade/vim-gitgutter",
		"hrsh7th/vim-vsnip",
		"nvim-telescope/telescope-ui-select.nvim",
		{
			"folke/which-key.nvim",
			event = "VeryLazy",
			init = function()
				vim.o.timeout = true
				vim.o.timeoutlen = 300
			end,
		},
		"gelguy/wilder.nvim",
		"laytan/cloak.nvim",
	},
	{
		lockfile = vim.fn.stdpath("data") .. "/lazy-lock.json",
	}
)

vim.api.nvim_create_autocmd('BufReadPost', {
	group = vim.g.event,
	callback = function(args)
		local valid_line = vim.fn.line([['"]]) >= 1 and vim.fn.line([['"]]) < vim.fn.line('$')
		local not_commit = vim.b[args.buf].filetype ~= 'commit'

		if valid_line and not_commit then
			vim.cmd([[normal! g`"]])
		end
	end,
})

require("telescope").setup {
	-- defaults = vim.tbl_extend(
	-- "force",
	-- require('telescope.themes').get_dropdown(), -- or get_cursor, get_ivy
	-- {
	-- }
	-- ),
	extensions = {
		["ui-select"] = {
			require("telescope.themes").get_dropdown {
				-- even more opts
			}

			-- pseudo code / specification for writing custom displays, like the one
			-- for "codeactions"
			-- specific_opts = {
			--   [kind] = {
			--     make_indexed = function(items) -> indexed_items, width,
			--     make_displayer = function(widths) -> displayer
			--     make_display = function(displayer) -> function(e)
			--     make_ordinal = function(e) -> string
			--   },
			--   -- for example to disable the custom builtin "codeactions" display
			--      do the following
			--   codeactions = false,
			-- }
		}
	}
}
require("telescope").load_extension("ui-select")
require("toggleterm").setup {}
require 'nvim-tree'.setup {}

vim.o.mouse = "a"
vim.o.number = true

vim.o.termguicolors = true
vim.o.number = true
vim.o.relativenumber = true

vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.softtabstop = 4
vim.o.expandtab = false
vim.o.smartindent = true
vim.o.smarttab = true
vim.o.scrolloff = 8

vim.o.undodir = vim.fn.stdpath('config') .. '/undo'


vim.keymap.set("n", "<leader>n", ":NvimTreeToggle<cr>", { noremap = true, silent = true })

vim.cmd.colorscheme "catppuccin-mocha"
local colors = require("catppuccin.palettes").get_palette()

local TelescopeColor = {
	TelescopeMatching = { fg = colors.flamingo },
	TelescopeSelection = { fg = colors.text, bg = colors.surface0, bold = true },

	TelescopePromptPrefix = { bg = colors.surface0 },
	TelescopePromptNormal = { bg = colors.surface0 },
	TelescopeResultsNormal = { bg = colors.mantle },
	TelescopePreviewNormal = { bg = colors.mantle },
	TelescopePromptBorder = { bg = colors.surface0, fg = colors.surface0 },
	TelescopeResultsBorder = { bg = colors.mantle, fg = colors.mantle },
	TelescopePreviewBorder = { bg = colors.mantle, fg = colors.mantle },
	TelescopePromptTitle = { bg = colors.pink, fg = colors.mantle },
	TelescopeResultsTitle = { fg = colors.mantle },
	TelescopePreviewTitle = { bg = colors.green, fg = colors.mantle },
}

for hl, col in pairs(TelescopeColor) do
	vim.api.nvim_set_hl(0, hl, col)
end

local lsp = require('lsp-zero').preset({})

local lspconfig = require('lspconfig')

require("mason").setup {}

require("mason-lspconfig").setup {
	ensure_installed = { "rust_analyzer", "clangd", }
}
require('mason-lspconfig').setup_handlers({
	function(server)
		lspconfig[server].setup({})
	end,
})

-- lsp.preset('recommended')
-- lsp.nvim_workspace()

lsp.on_attach(function(_, bufnr)
	lsp.default_keymaps({ buffer = bufnr })
	vim.keymap.set("n", "<leader>c", vim.lsp.buf.code_action, { noremap = true, silent = true })
	vim.keymap.set("n", "gs", require('telescope.builtin').lsp_document_symbols, { noremap = true, silent = true })
	vim.keymap.set("n", "gS", require('telescope.builtin').lsp_workspace_symbols, { noremap = true, silent = true })
	vim.keymap.set("n", "gd", require('telescope.builtin').lsp_definitions, { noremap = true, silent = true })
	vim.keymap.set("n", "gi", require('telescope.builtin').lsp_implementations, { noremap = true, silent = true })
	vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, { noremap = true, silent = true })
	vim.keymap.set("n", "<leader>;", vim.lsp.buf.format, { noremap = true, silent = true })
	lsp.buffer_autoformat()
end)


require('lspconfig').rust_analyzer.setup({
	settings = {
		['rust-analyzer'] = {
			checkOnSave = {
				command = "clippy",
			},
		}
	}
})

lsp.setup()

local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()

require('luasnip.loaders.from_vscode').lazy_load()

cmp.setup({
	sources = {
		{ name = 'nvim_lsp' },
		{ name = 'luasnip' },
	},
	mapping = {
		['<CR>'] = cmp.mapping.confirm({ select = false }),
	}
})

local wilder = require('wilder')
wilder.setup({ modes = { ':', '/', '?' } })
wilder.set_option('renderer', wilder.renderer_mux({
	[':'] = wilder.popupmenu_renderer({
		highlighter = wilder.basic_highlighter(),
	}),
}))

wilder.set_option('renderer', wilder.wildmenu_renderer({
	-- use wilder.wildmenu_lightline_theme() if using Lightline
	['/'] = wilder.wildmenu_airline_theme({
		-- highlights can be overriden, see :h wilder#wildmenu_renderer()
		highlights = { default = 'StatusLine' },
		highlighter = wilder.basic_highlighter(),
		separator = ' · ',
	}),
	['?'] = wilder.wildmenu_airline_theme({
		-- highlights can be overriden, see :h wilder#wildmenu_renderer()
		highlights = { default = 'StatusLine' },
		highlighter = wilder.basic_highlighter(),
		separator = ' · ',
	}),
}))

require 'nvim-treesitter.configs'.setup {
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
	},
}

require 'treesitter-context'.setup {
	enable = true,         -- Enable this plugin (Can be enabled/disabled later via commands)
	max_lines = 0,         -- How many lines the window should span. Values <= 0 mean no limit.
	min_window_height = 0, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
	line_numbers = true,
	multiline_threshold = 20, -- Maximum number of lines to collapse for a single context line
	trim_scope = 'outer',  -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
	mode = 'topline',      -- Line used to calculate context. Choices: 'cursor', 'topline'
	-- Separator between context and content. Should be a single character string, like '-'.
	-- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
	separator = nil,
	zindex = 20,  -- The Z-index of the context window
	on_attach = nil, -- (fun(buf: integer): boolean) return false to disable attaching
}

vim.keymap.set("n", "<leader>ff", require('telescope.builtin').find_files, { noremap = true, silent = true })
vim.keymap.set("n", "<leader>fg", require('telescope.builtin').live_grep, { noremap = true, silent = true })
vim.keymap.set("n", "<leader>fb", require('telescope.builtin').buffers, { noremap = true, silent = true })
vim.keymap.set("n", "<leader>fh", require('telescope.builtin').help_tags, { noremap = true, silent = true })

vim.keymap.set("n", "gl", require('telescope.builtin').diagnostics, { noremap = true, silent = true })
vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, { noremap = true, silent = true, buffer = bufnr })

require('cloak').setup({
	enabled = true,
	cloak_character = '*',
	-- The applied highlight group (colors) on the cloaking, see `:h highlight`.
	highlight_group = 'Comment',
	-- Applies the length of the replacement characters for all matched
	-- patterns, defaults to the length of the matched pattern.
	cloak_length = nil, -- Provide a number if you want to hide the true length of the value.
	-- Wether it should try every pattern to find the best fit or stop after the first.
	try_all_patterns = true,
	patterns = {
		{
			-- Match any file starting with '.env'.
			-- This can be a table to match multiple file patterns.
			file_pattern = '.env*',
			-- Match an equals sign and any character after it.
			-- This can also be a table of patterns to cloak,
			-- example: cloak_pattern = { ':.+', '-.+' } for yaml files.
			cloak_pattern = '=.+',
			-- A function, table or string to generate the replacement.
			-- The actual replacement will contain the 'cloak_character'
			-- where it doesn't cover the original text.
			-- If left emtpy the legacy behavior of keeping the first character is retained.
			replace = nil,
		},
	},
})

local wk = require("which-key")
wk.register(vim.mappings, vim.opts)
